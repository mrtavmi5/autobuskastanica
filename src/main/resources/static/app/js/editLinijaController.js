autobuskaStanicaApp.controller("EditLinijaController", function(
  $scope,
  $location,
  $routeParams,
  linijaService
) {
  $scope.linija = {
    brojMesta: "",
    cena: "",
    destinacija: "",
    vremePolaska: "",
    prevoznikId: "",
    vremeDate: null
  };
  $scope.prevoznici = [];
  $scope.editLinija = function(linija) {
    linija.id = $routeParams.id;
    linijaService.editLinija(linija).then(function() {
      $location.path("/");
    });
  };
  linijaService.getPrevoznici().then(function(data) {
    $scope.prevoznici = data;
  });

  linijaService.getLinija($routeParams.id).then(function(linija) {
    linija.prevoznikId = linija.prevoznik.id;
    linija.vremeDate = moment(linija.vremePolaska, "hh:mm").toDate();
    $scope.linija = linija;
  });

  $scope.updateVremePolaska = function() {
    $scope.linija.vremePolaska = moment($scope.linija.vremeDate).format(
      "hh:mm"
    );
  };
});
