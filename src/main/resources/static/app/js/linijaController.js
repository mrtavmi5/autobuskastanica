autobuskaStanicaApp.controller("LinijaController", function(
  $scope,
  linijaService
) {
  $scope.linije = [];
  $scope.linija = {
    brojMesta: "",
    cena: "",
    destinacija: "",
    vremePolaska: "",
    prevoznikId: "",
    vremeDate: new Date()
  };
  $scope.search = {
    prevoznikId: null,
    destinacija: null,
    maxCena: null,
    page: 1
  };
  $scope.prevoznici = [];
  $scope.getLinije = function(resetPage) {
    if (resetPage) {
      $scope.search.page = 1;
    }
    linijaService.getLinije($scope.search).then(function(data) {
      $scope.linije = data.content;
      $scope.totalPages = data.totalPages;
      $scope.totalElements = data.totalElements;
    });
  };
  $scope.deleteLinija = function(id) {
    linijaService.deleteLinija(id).then(function() {
      $scope.getLinije();
    });
  };
  $scope.addLinija = function(linija) {
    linija.id = 0;
    linijaService.addLinija(linija).then(function() {
      $scope.getLinije();
    });
  };
  $scope.nextPage = function() {
    $scope.search.page++;
    if ($scope.search.page > $scope.totalPages) {
      $scope.search.page = $scope.totalPages;
    }
    $scope.getLinije();
  };

  $scope.prevPage = function() {
    $scope.search.page--;
    if ($scope.search.page < 1) {
      $scope.search.page = 1;
    }
    $scope.getLinije();
  };

  $scope.reserve = function(id) {
    linijaService.reserve(id).then(function() {
      $scope.getLinije();
    });
  };

  linijaService.getPrevoznici().then(function(data) {
    $scope.prevoznici = data;
  });
  $scope.getLinije();

  $scope.updateVremePolaska = function() {
    $scope.linija.vremePolaska = moment($scope.linija.vremeDate).format(
      "hh:mm"
    );
  };
});
