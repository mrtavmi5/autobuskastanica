autobuskaStanicaApp.service("linijaService", function($http) {
  this.getLinije = function(search) {
    return $http
      .get("/api/linije", {
        params: search
      })
      .then(function(response) {
        return response.data;
      });
  };
  this.getLinija = function(id) {
    return $http.get("/api/linije/" + id).then(function(response) {
      return response.data;
    });
  };
  this.deleteLinija = function(id) {
    return $http.delete("/api/linije/" + id).then(function(response) {
      return response.data;
    });
  };
  this.addLinija = function(linija) {
    linija.id = 0;
    return $http.post("/api/linije", linija).then(function(response) {
      return response.data;
    });
  };
  this.editLinija = function(linija) {
    return $http
      .put("/api/linije/" + linija.id, linija)
      .then(function(response) {
        return response.data;
      });
  };

  this.reserve = function(id) {
    return $http.post("/api/linije/" + id).then(function(response) {
      return response.data;
    });
  };

  this.getPrevoznici = function() {
    return $http.get("/api/prevoznici").then(function(response) {
      return response.data;
    });
  };
});
