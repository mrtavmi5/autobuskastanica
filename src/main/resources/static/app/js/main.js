var autobuskaStanicaApp = angular.module("autobuskaStanicaApp", [
  "ngRoute",
  "ui.bootstrap"
]);

autobuskaStanicaApp.config([
  "$routeProvider",
  function($routeProvider) {
    $routeProvider
      .when("/", {
        templateUrl: "/app/html/linije.html",
        controller: "LinijaController"
      })
      .when("/edit/:id", {
        templateUrl: "/app/html/editLinija.html",
        controller: "EditLinijaController"
      })
      .otherwise({
        redirectTo: "/"
      });
  }
]);
