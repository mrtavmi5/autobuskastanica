package jwd.autobuskastanica.service;

import org.springframework.data.domain.Page;

import jwd.autobuskastanica.model.Linija;

public interface LinijaService {

    Linija findOne(Long id);

    Page<Linija> findAll(int page, String destinacija, Long prevoznikId, Double maxCena);

    Linija save(Linija linija);

    void remove(Long id);
}
