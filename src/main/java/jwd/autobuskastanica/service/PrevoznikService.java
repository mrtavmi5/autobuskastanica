package jwd.autobuskastanica.service;

import jwd.autobuskastanica.model.Prevoznik;

public interface PrevoznikService {
    
    Iterable<Prevoznik> findAll();

    Prevoznik save(Prevoznik prevoznik);
}
