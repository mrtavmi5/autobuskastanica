package jwd.autobuskastanica.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import jwd.autobuskastanica.model.Linija;
import jwd.autobuskastanica.repository.LinijaRepository;
import jwd.autobuskastanica.service.LinijaService;

@Service
@Transactional
public class JpaLinijaService implements LinijaService {

    private final static int pageSize = 10;

    @Autowired
    private LinijaRepository linijaRepository;

    @Override
    public Linija findOne(Long id) {
	return linijaRepository.findById(id).get();
    }

    @Override
    public Page<Linija> findAll(int page, String destinacija, Long prevoznikId, Double maxCena) {
	Pageable pageable = PageRequest.of(page, pageSize, Sort.by("id"));
	return linijaRepository.search("%" + destinacija + "%", prevoznikId, maxCena, pageable);
    }

    @Override
    public Linija save(Linija linija) {
	return linijaRepository.save(linija);
    }

    @Override
    public void remove(Long id) {
	linijaRepository.deleteById(id);

    }

}
