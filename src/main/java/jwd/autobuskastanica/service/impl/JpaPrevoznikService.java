package jwd.autobuskastanica.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jwd.autobuskastanica.model.Prevoznik;
import jwd.autobuskastanica.repository.PrevoznikRepository;
import jwd.autobuskastanica.service.PrevoznikService;

@Service
@Transactional
public class JpaPrevoznikService implements PrevoznikService {

    @Autowired
    private PrevoznikRepository prevoznikRepository;

    @Override
    public Iterable<Prevoznik> findAll() {
	return prevoznikRepository.findAll();
    }

    @Override
    public Prevoznik save(Prevoznik prevoznik) {
	return prevoznikRepository.save(prevoznik);
    }

}
