package jwd.autobuskastanica;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import jwd.autobuskastanica.model.Linija;
import jwd.autobuskastanica.model.Prevoznik;
import jwd.autobuskastanica.service.LinijaService;
import jwd.autobuskastanica.service.PrevoznikService;

@Component
public class TestData {

    @Autowired
    private PrevoznikService prevoznikService;
    
    @Autowired
    private LinijaService linijaService;
    @PostConstruct
    public void init() {
	Prevoznik pre1 = new Prevoznik("Lasta", "Beograd", "123456");
	pre1 = prevoznikService.save(pre1);
	Prevoznik pre2 = new Prevoznik("Sirmium", "Sremska Mitrovica", "111111");
	pre2 = prevoznikService.save(pre2);
	Prevoznik pre3 = new Prevoznik("Borovica", "Ruma", "121212");
	pre3 = prevoznikService.save(pre3);
	Prevoznik pre4 = new Prevoznik("Strela", "Ub", "234567");
	pre4 = prevoznikService.save(pre4);
	Prevoznik pre5 = new Prevoznik("Master Bus", "Novi Sad", "654321");
	pre5 = prevoznikService.save(pre5);
	
	linijaService.save(new Linija(70, 220, "09:30", "Novi Sad", pre1));
	linijaService.save(new Linija(50, 350, "10:30", "Beograd", pre1));
	linijaService.save(new Linija(55, 300, "11:00", "Subotica", pre1));
	linijaService.save(new Linija(60, 200, "12:20", "Ruma", pre2));
	linijaService.save(new Linija(65, 150, "13:35", "Valjevo", pre2));
	linijaService.save(new Linija(70, 190, "14:15", "Irig", pre2));
	linijaService.save(new Linija(70, 460, "15:00", "Kragujevac", pre3));
	linijaService.save(new Linija(70, 600, "16:30", "Novi Pazar", pre3));
	linijaService.save(new Linija(70, 900, "17:00", "Nis", pre3));
	linijaService.save(new Linija(20, 320, "11:30", "Novi Sad", pre4));
	linijaService.save(new Linija(40, 450, "03:35", "Valjevo", pre4));
	linijaService.save(new Linija(60, 760, "14:00", "Kragujevac", pre4));
	linijaService.save(new Linija(50, 750, "19:30", "Beograd", pre5));
	linijaService.save(new Linija(60, 90, "11:55", "Irig", pre5));
    }
}
