package jwd.autobuskastanica.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import jwd.autobuskastanica.model.Linija;

@Repository
public interface LinijaRepository extends PagingAndSortingRepository<Linija, Long> {

    @Query("SELECT l FROM Linija l " + "WHERE (:destinacija IS NULL OR l.destinacija LIKE :destinacija) "
	    + "AND (:prevoznikId IS NULL OR l.prevoznik.id = :prevoznikId) "
	    + "AND (:maxCena IS NULL OR l.cena <= :maxCena)")
    Page<Linija> search(@Param("destinacija") String destinacija, @Param("prevoznikId") Long prevoznikId,
	    @Param("maxCena") Double maxCena, Pageable pageable);
}
