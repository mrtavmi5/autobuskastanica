package jwd.autobuskastanica.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import jwd.autobuskastanica.model.Prevoznik;
@Repository
public interface PrevoznikRepository extends PagingAndSortingRepository<Prevoznik, Integer> {

}
