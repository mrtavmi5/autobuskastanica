package jwd.autobuskastanica.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
@Entity
@Table(name="tblPrevoznik")
public class Prevoznik {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;
    @NotBlank
    @Column(unique=true, length=30)
    private String naziv;
    @Column
    private String adresa;
    @NotBlank
    @Column(unique=true, length=30)
    private String pib;
    @OneToMany(mappedBy="prevoznik", fetch=FetchType.LAZY)
    private List<Linija> linije = new ArrayList<>();
   
    public Prevoznik() {
    }

    public Prevoznik(String naziv, String adresa, String pib) {
	this.naziv = naziv;
	this.adresa = adresa;
	this.pib = pib;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getPib() {
        return pib;
    }

    public void setPib(String pib) {
        this.pib = pib;
    }

    public List<Linija> getLinije() {
        return linije;
    }

    public void setLinije(List<Linija> linije) {
        this.linije = linije;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof Prevoznik))
	    return false;
	Prevoznik other = (Prevoznik) obj;
	if (id != other.id)
	    return false;
	return true;
    }
    
    
    
}
