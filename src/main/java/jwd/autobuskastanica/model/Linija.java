package jwd.autobuskastanica.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "tblLinija")
public class Linija {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;
    @NotNull
    @Column
    private int brojMesta;
    @Column
    private double cena;
    @Column
    private String vremePolaska;
    @NotBlank
    @Column
    @NotNull
    private String destinacija;
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    private Prevoznik prevoznik;

    public Linija() {
    }

    public Linija(int brMesta, int cena, String vremePol, String destinacija, Prevoznik prevoznik) {
	this.brojMesta = brMesta;
	this.cena = cena;
	this.vremePolaska = vremePol;
	this.destinacija = destinacija;
	this.prevoznik = prevoznik;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public int getBrojMesta() {
	return brojMesta;
    }

    public void setBrojMesta(int brMesta) {
	this.brojMesta = brMesta;
    }

    public double getCena() {
	return cena;
    }

    public void setCena(double cena) {
	this.cena = cena;
    }

    public String getVremePolaska() {
	return vremePolaska;
    }

    public void setVremePolaska(String vremePol) {
	this.vremePolaska = vremePol;
    }

    public String getDestinacija() {
	return destinacija;
    }

    public void setDestinacija(String destinacija) {
	this.destinacija = destinacija;
    }

    public Prevoznik getPrevoznik() {
	return prevoznik;
    }

    public void setPrevoznik(Prevoznik prevoznik) {
	this.prevoznik = prevoznik;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (!(obj instanceof Linija))
	    return false;
	Linija other = (Linija) obj;
	if (id != other.id)
	    return false;
	return true;
    }

}
