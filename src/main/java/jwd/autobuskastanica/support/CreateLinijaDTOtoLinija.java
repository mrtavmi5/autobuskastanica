package jwd.autobuskastanica.support;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.autobuskastanica.model.Linija;
import jwd.autobuskastanica.model.Prevoznik;
import jwd.autobuskastanica.web.dto.CreateLinijaDTO;
@Component
public class CreateLinijaDTOtoLinija implements Converter<CreateLinijaDTO, Linija> {

    @Override
    public Linija convert(CreateLinijaDTO source) {
	Linija linija = new Linija();
	linija.setId(source.getId());
	linija.setBrojMesta(source.getBrojMesta());
	linija.setCena(source.getCena());
	linija.setDestinacija(source.getDestinacija());
	linija.setVremePolaska(source.getVremePolaska());
	Prevoznik pr = new Prevoznik();
	pr.setId(source.getPrevoznikId());
	linija.setPrevoznik(pr);

	return linija;
    }

}
