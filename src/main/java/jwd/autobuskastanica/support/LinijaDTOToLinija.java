package jwd.autobuskastanica.support;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.autobuskastanica.model.Linija;
import jwd.autobuskastanica.web.dto.LinijaDTO;

@Component
public class LinijaDTOToLinija implements Converter<LinijaDTO, Linija> {

    @Override
    public Linija convert(LinijaDTO source) {
	Linija linija = new Linija();
	linija.setId(source.getId());
	linija.setCena(source.getCena());
	linija.setBrojMesta(source.getBrojMesta());
	linija.setDestinacija(source.getDestinacija());
	linija.setVremePolaska(source.getVremePolaska());

	return linija;
    }

}
