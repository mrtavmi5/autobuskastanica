package jwd.autobuskastanica.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import jwd.autobuskastanica.model.Linija;
import jwd.autobuskastanica.web.dto.LinijaDTO;

@Component
public class LinijaToLinijaDTO implements Converter<Linija, LinijaDTO> {

    @Autowired
    PrevoznikToPrevoznikDTO prevoznikToPrevoznikDTO;
    
	@Override
    public LinijaDTO convert(Linija source) {
	LinijaDTO dto = new LinijaDTO();
	dto.setId(source.getId());
	dto.setCena(source.getCena());
	dto.setBrojMesta(source.getBrojMesta());
	dto.setDestinacija(source.getDestinacija());
	dto.setVremePolaska(source.getVremePolaska());
	dto.setPrevoznik(prevoznikToPrevoznikDTO.convert(source.getPrevoznik()));
	
	return dto;
    }
    
    public List<LinijaDTO> convert(List<Linija> linije) {

	List<LinijaDTO> dtos = new ArrayList<LinijaDTO>();
	for (Linija linija : linije) {
		dtos.add(convert(linija));
	}

	return dtos;
}

    public Page<LinijaDTO> convert(Page<Linija> linije) {
	List<Linija> linijeList = linije.getContent();
	Pageable pageable = linije.getPageable();
	Long total = linije.getTotalElements();
	List<LinijaDTO> dtoList = convert(linijeList);
	return new PageImpl<>(dtoList, pageable, total);
}
}
