package jwd.autobuskastanica.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.autobuskastanica.model.Prevoznik;
import jwd.autobuskastanica.web.dto.PrevoznikDTO;

@Component
public class PrevoznikToPrevoznikDTO implements Converter<Prevoznik, PrevoznikDTO> {

    @Override
    public PrevoznikDTO convert(Prevoznik source) {
	PrevoznikDTO dto = new PrevoznikDTO();
	dto.setId(source.getId());
	dto.setNaziv(source.getNaziv());
	dto.setAdresa(source.getAdresa());
	dto.setPib(source.getPib());

	return dto;
    }

    public Iterable<PrevoznikDTO> convert(Iterable<Prevoznik> prevoznici) {

	List<PrevoznikDTO> dtos = new ArrayList<PrevoznikDTO>();
	for (Prevoznik pre : prevoznici) {
	    dtos.add(convert(pre));
	}

	return dtos;
    }
}
