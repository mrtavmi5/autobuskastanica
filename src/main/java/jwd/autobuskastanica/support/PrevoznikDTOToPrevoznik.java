package jwd.autobuskastanica.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import jwd.autobuskastanica.model.Prevoznik;
import jwd.autobuskastanica.web.dto.PrevoznikDTO;

@Component
public class PrevoznikDTOToPrevoznik implements Converter<PrevoznikDTO, Prevoznik> {

    @Override
    public Prevoznik convert(PrevoznikDTO source) {
	Prevoznik pre = new Prevoznik();
	pre.setId(source.getId());
	pre.setNaziv(source.getNaziv());
	pre.setAdresa(source.getAdresa());
	pre.setPib(source.getPib());

	return pre;
    }

    public Iterable<Prevoznik> convert(List<PrevoznikDTO> dtos) {

	List<Prevoznik> prevoznici = new ArrayList<Prevoznik>();
	for (PrevoznikDTO dto : dtos) {
	    prevoznici.add(convert(dto));
	}

	return prevoznici;
    }

}
