package jwd.autobuskastanica.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class PrevoznikDTO {

    @NotNull
    private Long id;
    @NotBlank
    private String naziv;
    @NotBlank
    private String adresa;
    @NotBlank
    private String pib;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getNaziv() {
	return naziv;
    }

    public void setNaziv(String naziv) {
	this.naziv = naziv;
    }

    public String getAdresa() {
	return adresa;
    }

    public void setAdresa(String adresa) {
	this.adresa = adresa;
    }

    public String getPib() {
	return pib;
    }

    public void setPib(String pib) {
	this.pib = pib;
    }

}
