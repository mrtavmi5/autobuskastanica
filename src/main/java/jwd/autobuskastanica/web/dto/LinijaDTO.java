package jwd.autobuskastanica.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class LinijaDTO {

    @NotNull
    private Long id;
    @NotNull
    private int brojMesta;
    private double cena;
    private String vremePolaska;
    @NotBlank
    @NotNull
    private String destinacija;
    @NotNull
    private PrevoznikDTO prevoznik;

    public PrevoznikDTO getPrevoznik() {
	return prevoznik;
    }

    public void setPrevoznik(PrevoznikDTO prevoznik) {
	this.prevoznik = prevoznik;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public int getBrojMesta() {
	return brojMesta;
    }

    public void setBrojMesta(int brMesta) {
	this.brojMesta = brMesta;
    }

    public double getCena() {
	return cena;
    }

    public void setCena(double cena) {
	this.cena = cena;
    }

    public String getVremePolaska() {
	return vremePolaska;
    }

    public void setVremePolaska(String vremePol) {
	this.vremePolaska = vremePol;
    }

    public String getDestinacija() {
	return destinacija;
    }

    public void setDestinacija(String destinacija) {
	this.destinacija = destinacija;
    }

}
