package jwd.autobuskastanica.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class CreateLinijaDTO {

    @NotNull
    private Long id;
    @NotNull
    private int brojMesta;
    private int cena;
    private String vremePolaska;
    @NotBlank
    @NotNull
    private String destinacija;
    @NotNull
    private Long prevoznikId;

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public int getBrojMesta() {
	return brojMesta;
    }

    public void setBrojMesta(int brMesta) {
	this.brojMesta = brMesta;
    }

    public int getCena() {
	return cena;
    }

    public void setCena(int cena) {
	this.cena = cena;
    }

    public String getVremePolaska() {
	return vremePolaska;
    }

    public void setVremePolaska(String vremePol) {
	this.vremePolaska = vremePol;
    }

    public String getDestinacija() {
	return destinacija;
    }

    public void setDestinacija(String destinacija) {
	this.destinacija = destinacija;
    }

    public Long getPrevoznikId() {
	return prevoznikId;
    }

    public void setPrevoznikId(Long prevoznikId) {
	this.prevoznikId = prevoznikId;
    }
}
