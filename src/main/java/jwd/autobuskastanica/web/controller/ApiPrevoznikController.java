package jwd.autobuskastanica.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jwd.autobuskastanica.model.Prevoznik;
import jwd.autobuskastanica.service.PrevoznikService;
import jwd.autobuskastanica.support.PrevoznikDTOToPrevoznik;
import jwd.autobuskastanica.support.PrevoznikToPrevoznikDTO;
import jwd.autobuskastanica.web.dto.PrevoznikDTO;

@RestController
@RequestMapping(path = "/api/prevoznici")
public class ApiPrevoznikController {

    @Autowired
    private PrevoznikService prevoznikService;
    @Autowired
    private PrevoznikToPrevoznikDTO prevoznikToPrevoznikDTO;
    @Autowired
    private PrevoznikDTOToPrevoznik prevoznikDTOToPrevoznik;

    @GetMapping
    public Iterable<PrevoznikDTO> getAll() {
	return prevoznikToPrevoznikDTO.convert(prevoznikService.findAll());
    }

    @PostMapping
    public PrevoznikDTO create(@Validated @RequestBody PrevoznikDTO dto) {
	Prevoznik pre = prevoznikDTOToPrevoznik.convert(dto);
	pre = prevoznikService.save(pre);
	return prevoznikToPrevoznikDTO.convert(pre);
    }

}
