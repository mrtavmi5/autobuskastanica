package jwd.autobuskastanica.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jwd.autobuskastanica.model.Linija;
import jwd.autobuskastanica.service.LinijaService;
import jwd.autobuskastanica.support.CreateLinijaDTOtoLinija;
import jwd.autobuskastanica.support.LinijaToLinijaDTO;
import jwd.autobuskastanica.web.dto.CreateLinijaDTO;
import jwd.autobuskastanica.web.dto.LinijaDTO;

@RestController
@RequestMapping(path = "/api/linije")
public class ApiLinijaController {

    @Autowired
    private LinijaService linijaService;
    @Autowired
    private LinijaToLinijaDTO linijaToLinijaDTO;
    @Autowired
    private CreateLinijaDTOtoLinija createLinijaDTOToLinija;

    @GetMapping
    public Page<LinijaDTO> getAll(@RequestParam(required = false, defaultValue = "1") int page,
	    @RequestParam(required = false, defaultValue = "") String destinacija,
	    @RequestParam(required = false) Long prevoznikId, @RequestParam(required = false) Double maxCena) {
	page--;
	return linijaToLinijaDTO.convert(linijaService.findAll(page, destinacija, prevoznikId, maxCena));
    }

    @GetMapping(path = "/{id}")
    public LinijaDTO get(@PathVariable Long id) {
	return linijaToLinijaDTO.convert(linijaService.findOne(id));
    }

    @PostMapping
    public LinijaDTO create(@Validated @RequestBody CreateLinijaDTO dto) {
	Linija lin = createLinijaDTOToLinija.convert(dto);
	lin.setId(null);
	lin = linijaService.save(lin);
	return linijaToLinijaDTO.convert(lin);
    }
    
    @PostMapping(path = "/{id}")
    public void reserve(@PathVariable Long id) {
	Linija lin = linijaService.findOne(id);
	int mesto = lin.getBrojMesta();
	mesto--;
	lin.setBrojMesta(mesto);
	linijaService.save(lin);
    }

    @PutMapping(path = "/{id}")
    public LinijaDTO edit(@PathVariable Long id, @RequestBody CreateLinijaDTO dto) {
	dto.setId(id);
	Linija lin = createLinijaDTOToLinija.convert(dto);
	lin = linijaService.save(lin);
	return linijaToLinijaDTO.convert(lin);
    }

    @DeleteMapping(path = "/{id}")
    public void delete(@PathVariable Long id) {
	linijaService.remove(id);
    }
}
